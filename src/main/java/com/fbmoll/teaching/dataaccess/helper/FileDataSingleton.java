package com.fbmoll.teaching.dataaccess.helper;

import com.fbmoll.teaching.dataaccess.data.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.List;

public class FileDataSingleton {
    private static final Logger log = LoggerFactory.getLogger(FileUtilsHelper.class);
    private static FileDataSingleton instance = null;

    public static FileDataSingleton getInstance() {
        if (instance == null) {
            instance = new FileDataSingleton();
        }
        return instance;
    }

    public Document generateRandomProductXML() {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();

            Element rootElement = doc.createElement("products");
            DummyUtils dummyUtils = new DummyUtils();
            List<Product> products = dummyUtils.getProducts();
            for (Product item : products) {
                Element aux = doc.createElement("product");
                aux.setAttribute("value", item.getValue().toString());
                aux.setTextContent(item.getName());
                rootElement.appendChild(aux);
            }
            doc.appendChild(rootElement);
            return doc;
        } catch (ParserConfigurationException e) {
            log.error("Error creating XML", e);
            return null;
        }
    }

    public File saveDocumentAsFile(String path, Document doc, boolean showOutput) {
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource src = new DOMSource(doc);
            File file = new File(path);
            StreamResult result = new StreamResult(path);
            transformer.transform(src, result);
            if (showOutput) {
                StreamResult consoleResult = new StreamResult(System.out);
                transformer.transform(src, consoleResult);
            }
            return file;
        } catch (Exception e) {
            log.error("Error saving XMl file", e);
            return null;
        }
    }

    public void serializeContent(String path, Object o) {
        ObjectOutputStream serializer = null;
        try {
            if (o != null) {
                serializer = new ObjectOutputStream((new FileOutputStream(path)));
                serializer.writeObject(o);
            }
        } catch (IOException e) {
            log.error("Unable to serialize");
        } finally {
            if (serializer != null) {
                try {
                    serializer.close();
                } catch (IOException e) {
                    log.error("Unable to close serializer.");
                }
            }
        }
    }

    public <T extends Serializable> T deserialize(String path) {
        T o = null;
        ObjectInputStream deserializer = null;
        try {
            deserializer = new ObjectInputStream((new FileInputStream(path)));
            o = (T) deserializer.readObject();
        } catch (Exception e) {
            log.error("Unable to deserialize.");
        } finally {
            if (deserializer != null) {
                try {
                    deserializer.close();
                } catch (IOException e) {
                    log.error("Unable to close deserializer.");
                }
            }
        }
        return o;
    }
}
