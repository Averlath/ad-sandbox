package com.fbmoll.teaching.dataaccess.helper;

public enum ConfigurationProperties {
    Name, Password, Server, Port
}