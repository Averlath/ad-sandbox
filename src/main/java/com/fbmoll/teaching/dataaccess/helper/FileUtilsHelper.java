package com.fbmoll.teaching.dataaccess.helper;

import com.fbmoll.teaching.dataaccess.data.ConfigValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class FileUtilsHelper {
    private static final Logger log = LoggerFactory.getLogger(FileUtilsHelper.class);
    private static final String USER_HOME = System.getProperty("user.home");
    private static final String APP_PATH = "DataAccessFiles"; //TODO 2020: Change to project property

    /*public static File genUserFile(String fileName, String content) {
        try {
            String pathSeparator = System.getProperty("path.separator");
            String realFileName = StringUtils.substringAfterLast(fileName, pathSeparator);
            String fullFileName = String.format("%s\\%s\\%s", USER_HOME, APP_PATH, realFileName);
            return generateFile(fullFileName, content);
        } catch (Exception e) {
            log.error("File generation failed.", e);
            return null;
        }
    }*/

    public static File generateFile(String path, String content) {
        FileWriter fileWriter = null;
        File file = null;
        try {
            file = new File(path);
            file.getParentFile().mkdirs();
            fileWriter = new FileWriter(path);
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(content);
        } catch (IOException e) {
            log.error("i/o ERROR ", e);
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    log.error("i/o ERROR ", e);
                }
            }
            return file;
        }
    }

    public static List<String> readFileLines(String filePath) {
        ArrayList<String> rtn = new ArrayList<>();
        BufferedReader buffer = null;
        try {
            File file = new File(filePath);
            FileReader fileReader = new FileReader(file);
            buffer = new BufferedReader(fileReader);
            String fileLine;
            while ((fileLine = buffer.readLine()) != null) {
                rtn.add(fileLine);
            }
        } catch (Exception e) {
            log.error("Can't access to file", e);
        } finally {
            if (buffer != null)
                try {
                    buffer.close();
                } catch (Exception e) {
                    log.error("Can't close file", e);
                }
        }
        return rtn;
    }

    public ConfigValues loadProperties(String path) {
        try {
            ConfigValues configValues = new ConfigValues();
            Properties properties = new Properties();
            properties.load(new FileInputStream(path));
            configValues.setName(properties.getProperty(ConfigurationProperties.Name.toString()));
            configValues.setPassword(properties.getProperty(ConfigurationProperties.Password.toString()));
            configValues.setServer(properties.getProperty(ConfigurationProperties.Server.toString()));
            configValues.setPort(properties.getProperty(ConfigurationProperties.Port.toString()));
            return configValues;
        } catch (Exception e) {
            FileUtilsHelper.log.error("Loading properties.", e);
            return null;
        }
    }

    public void saveProperties(ConfigValues data, String path) {
        try {
            Properties properties = new Properties();
            properties.setProperty(ConfigurationProperties.Name.toString(), data.getName());
            properties.setProperty(ConfigurationProperties.Password.toString(), data.getPassword());
            properties.setProperty(ConfigurationProperties.Server.toString(), data.getServer());
            properties.setProperty(ConfigurationProperties.Port.toString(), data.getPort());
            properties.store(new FileOutputStream(path), "Properties file.");
        } catch (Exception e) {
            FileUtilsHelper.log.error("Saving properties.", e);
        }
    }
}