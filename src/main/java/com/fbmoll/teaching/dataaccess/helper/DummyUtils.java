package com.fbmoll.teaching.dataaccess.helper;

import com.fbmoll.teaching.dataaccess.data.Product;
import com.fbmoll.teaching.dataaccess.data.Student;
import org.jeasy.random.EasyRandom;

import java.util.ArrayList;
import java.util.List;

public class DummyUtils {
    public List<Student> genStudents(int size) {
        ArrayList<Student> arrData = new ArrayList<>();
        EasyRandom generator = new EasyRandom();
        for (int i = 0; i < size; i++) {
            arrData.add(generator.nextObject(Student.class));
        }
        return arrData;
    }

    public List<Product> getProducts() {
        int numItems = (int) (Math.random() * 10);
        ArrayList<Product> aux = new ArrayList<>();
        EasyRandom gen = new EasyRandom();
        for (int i = 0; i < numItems; i++) {
            aux.add(gen.nextObject(Product.class));
        }
        return aux;
    }
}
