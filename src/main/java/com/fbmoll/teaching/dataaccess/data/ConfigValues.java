package com.fbmoll.teaching.dataaccess.data;

import org.apache.commons.lang3.StringUtils;

public class ConfigValues {
    private String name;
    private String password;
    private String server;
    private String port;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().isAssignableFrom(this.getClass())) {
            ConfigValues value = (ConfigValues) obj;
            return StringUtils.equals(value.getName(), this.getName())
                    && StringUtils.equals(value.getPassword(), this.getPassword())
                    && StringUtils.equals(value.getServer(), this.getServer())
                    && StringUtils.equals(value.getPort(), this.getPort());
        }
        return super.equals(obj);
    }
}
