package com.fbmoll.teaching.dataaccess;

import com.fbmoll.teaching.dataaccess.data.ConfigValues;
import com.fbmoll.teaching.dataaccess.helper.DummyUtils;
import com.fbmoll.teaching.dataaccess.data.Student;
import com.fbmoll.teaching.dataaccess.helper.FileUtilsHelper;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.io.File;
import java.util.List;

@SpringBootTest
class DataAccessApplicationTests {
    private static final String APP_PATH = "DataAccessFiles"; //TODO 2020: Change to project property
    private static final String USER_HOME = System.getProperty("user.home");

    @Test
    void tryCreateFile() {
        String name = "test.txt";
        String fileName = String.format("%s\\%s\\%s", USER_HOME, APP_PATH, name);
        File file = FileUtilsHelper.generateFile(fileName, "dummy Content");
        Assert.notNull(file, "File is empty.");
    }

    @Test
    void tryCreateAndReadFile() {
        String name = "test.data";
        String fileName = String.format("%s\\%s\\%s", USER_HOME, APP_PATH, name);
        File file = FileUtilsHelper.generateFile(fileName, "Hello\nDon Simon");
        List<String> fileContent = FileUtilsHelper.readFileLines(fileName);
        Assert.notEmpty(fileContent, "Your file is empty.");
        Assert.isTrue(StringUtils.equals(fileContent.get(0),"Hello"),"First line does not match");
        Assert.isTrue(StringUtils.equals(fileContent.get(1),"Don Simon"),"Second line does not match");
    }

    @Test
    void validateProperties() {
        String name = "myprops.properties";
        String fileName = String.format("%s\\%s\\%s", USER_HOME, APP_PATH, name);
        FileUtilsHelper fileUtils = new FileUtilsHelper();
        ConfigValues data = new ConfigValues();
        data.setName("myUser");
        data.setPassword("myPassword");
        data.setServer("localhost");
        data.setPort("3306");
        fileUtils.saveProperties(data, fileName);
        ConfigValues properties = fileUtils.loadProperties(fileName);
        Assert.isTrue(StringUtils.equals(data.getName(), properties.getName()),
                "Name is different.");
        Assert.isTrue(StringUtils.equals(data.getPassword(), properties.getPassword()),
                "Password is different.");
        Assert.isTrue(StringUtils.equals(data.getServer(), properties.getServer()),
                "Server is different.");
        Assert.isTrue(StringUtils.equals(data.getPort(), properties.getPort()),
                "Port is different.");
        Assert.isTrue(data.equals(properties), "Properties are different.");
    }

    @Test
    void tryDummyGenerator() {
        DummyUtils dummyUtils = new DummyUtils();
        List<Student> data = dummyUtils.genStudents(50);
        Assert.notEmpty(data, "Students list is empty.");
    }
}