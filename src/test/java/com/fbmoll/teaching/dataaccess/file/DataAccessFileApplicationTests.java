package com.fbmoll.teaching.dataaccess.file;

import com.fbmoll.teaching.dataaccess.data.Product;
import com.fbmoll.teaching.dataaccess.helper.DummyUtils;
import com.fbmoll.teaching.dataaccess.helper.FileDataSingleton;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;
import org.w3c.dom.Document;

import java.io.File;
import java.util.List;

public class DataAccessFileApplicationTests {
    private static final String APP_PATH = "DataAccessFiles"; //TODO 2020: Change to project property
    private static final String USER_HOME = System.getProperty("user.home");

    @Test
    void storeProductsInXML() {
        FileDataSingleton fileDataSingleton = FileDataSingleton.getInstance();
        Document document = fileDataSingleton.generateRandomProductXML();
        String fileName = String.format("%s\\%s\\%s", USER_HOME, APP_PATH, "classic.xml");
        File file = fileDataSingleton.saveDocumentAsFile(fileName, document, true);
        Assert.notNull(file, "The file is empty.");
    }

    @Test
    void trySerializer() { //TODO Fails
        DummyUtils dummyUtils = new DummyUtils();
        String fileName = String.format("%s\\%s\\%s", USER_HOME, APP_PATH, "serial.data");
        List<Product> products = dummyUtils.getProducts();
        FileDataSingleton.getInstance().serializeContent(fileName, products);
        Assert.notEmpty(products, "Products list is empty.");
        List<Product> returnProducts = (List<Product>) FileDataSingleton
                .getInstance().deserialize(fileName);
        Assert.notEmpty(returnProducts, "Product list is empty");
        Assert.isTrue(products.equals(returnProducts), "They are different.");
    }
}